# Warehouse-Backend

## Introduction
This repo contains Event Driven Microservice Arhitecture example for simple Warehouse use-case.

## Tech Stack
 - GCP
 - K8s
 - Spring WebFlux
 - RabbitMQ
 - ELK Stack

## API Docs
Check api docs folder for OpenAPI Specs.

Note: Postman collection also included in API Docs

## Working DEMO Environment
Already deployed to GCP and will be available for a couple of days.

- API GW URL: http://34.147.41.102:80
- KIBANA URL: http://34.90.100.36:5601/

Not1: No authentication required.
Not2: You can use provided postman collections to interact with the APIs.
Not3: All APIs are responding with Request-ID in the response header, if you copy the RequestID and search for it in Kibana you can track the logs for particular request.
