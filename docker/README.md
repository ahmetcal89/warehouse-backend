# Docker-Compose for Local Development

## Introduction
Docker-compose file to support local development. Also used for building and publishing images to GCP Container Registry.

## Tech Stack
 - docker

## Local Development
### Preconditions
 - docker-compose

 ### Usage
``` shell
# Run Local Environment
docker-compose up -d

# Publish Images to GCP Container Registry
docker-compose build
docker-compose push discovery-service gateway-service order-service stock-service
```
