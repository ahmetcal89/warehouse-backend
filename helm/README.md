# Warehouse Backend HELM Deployment Charts

## Introduction
Helm project to maintain Warehouse Backend Services

Warehouse backend services deployed with ELK Stack and RabbitMQ

## Tech Stack
 - docker
 - helm
 - K8s

## Local Development
### Preconditions
 - gcloud cli
 - kubectl
 - helm
 
 ### Usage
``` shell
# Navigate to environments folder
cd helm/environments

# Update helm chart dependencies
./update_dependencies.sh

# Install for dev environemt
helm install warehouse-dev dev-env

# Upgrade for dev environemt
helm upgrade warehouse-dev dev-env

# Remove dev environment
helm delete warehouse-dev
```
