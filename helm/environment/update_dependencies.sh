#!/bin/bash -e

cd ${WORKSPACE}/helm/warehouse-common
helm dependencies build
cd ${WORKSPACE}/helm/warehouse-services/discovery-service
helm dependencies build
cd ${WORKSPACE}/helm/warehouse-services/config-service
helm dependencies build
cd ${WORKSPACE}/helm/warehouse-services/gateway-service
helm dependencies build
cd ${WORKSPACE}/helm/warehouse-services/order-service
helm dependencies build
cd ${WORKSPACE}/helm/warehouse-services/stock-service
helm dependencies build
cd ${WORKSPACE}/helm/elasticsearch
helm dependencies build
cd ${WORKSPACE}/helm/kibana
helm dependencies build
cd ${WORKSPACE}/helm/logstash
helm dependencies build
cd ${WORKSPACE}/helm/rabbitmq
helm dependencies build
cd ${WORKSPACE}/helm/environment/dev-env
helm dependencies build