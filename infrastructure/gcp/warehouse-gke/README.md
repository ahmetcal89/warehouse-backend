# Warehouse Backend Infrastructure

## Introduction
Terraform project to maintain GCP GKE Resources

## Tech Stack
 - GCP
 - Terraform (v1.2.3)

## Local Development
### Preconditions
 - gcloud cli
 - terraform (v1.2.3)
 
 ### Usage
``` shell
terraform init
terraform plan 
terraform apply 
terraform destroy 

```
