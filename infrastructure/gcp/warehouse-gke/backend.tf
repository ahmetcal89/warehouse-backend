terraform {
  backend "gcs" {
    bucket  = "terraform-warehouse-backend-remote-state"
    prefix  = "gke/state"
  }
}