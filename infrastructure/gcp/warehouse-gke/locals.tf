locals {
  cluster_type           = "warehouse-backend"
  network_name           = "warehouse-backend-network"
  subnet_name            = "warehouse-backend-subnet"
  master_auth_subnetwork = "warehouse-backend-master-subnet"
  pods_range_name        = "ip-range-pods-warehouse-public"
  svc_range_name         = "ip-range-svc-warehouse-public"
  subnet_names           = [for subnet_self_link in module.gcp-network.subnets_self_links : split("/", subnet_self_link)[length(split("/", subnet_self_link)) - 1]]
}
