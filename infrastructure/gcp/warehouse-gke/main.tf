provider "google" {
  project = "warehouse-backend-364020"
  region  = "europe-west4"
}

provider "kubernetes" {
  host                   = "https://${module.gke.endpoint}"
  token                  = data.google_client_config.default.access_token
  cluster_ca_certificate = base64decode(module.gke.ca_certificate)
}

data "google_client_config" "default" {}

module "gke" {
  source                            = "terraform-google-modules/kubernetes-engine/google//modules/beta-public-cluster/"
  version                           = "23.1.0"
  project_id                        = var.project_id
  name                              = "${local.cluster_type}-cluster"
  region                            = var.region
  network                           = module.gcp-network.network_name
  subnetwork                        = local.subnet_names[index(module.gcp-network.subnets_names, local.subnet_name)]
  ip_range_pods                     = local.pods_range_name
  ip_range_services                 = local.svc_range_name
  create_service_account            = false
  remove_default_node_pool          = false
  disable_legacy_metadata_endpoints = false
  cluster_autoscaling               = var.cluster_autoscaling
  default_max_pods_per_node         = 10

  node_pools = [
    {
      name               = "worker-pool"
      machine_type       = "n1-standard-2"
      node_locations     = "${var.region}-b,${var.region}-c"
      min_count          = 3
      max_count          = 5
      disk_type          = "pd-standard"
      disk_size_gb       = 32
      autoscaling        = true
      auto_upgrade       = false
      auto_repair        = false
    },
  ]

  node_pools_linux_node_configs_sysctls = {
    worker-pool = {
      "net.core.netdev_max_backlog" = "20000"
    }
  }
}

