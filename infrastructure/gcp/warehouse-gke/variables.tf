variable "project_id" {
  description = "The project ID to host the cluster in"
  type = string
  default = "warehouse-backend-364020"
}

variable "region" {
  description = "The region the cluster in"
  type = string
  default     = "europe-west4"
}
