# Warehouse-Backend Services

## Introduction

Order-Service: Managing incoming orders
Stock-Service: Managing Product Inventory
Discovery-Service: Eureka for Service Discovery
Gateway-Service: Spring Cloud Gateway as single entry point

## Tech Stack

- Java 17
- Spring WebFlux
- RabbitMQ
- gradle

## Local Development

### Preconditions

- gradle


