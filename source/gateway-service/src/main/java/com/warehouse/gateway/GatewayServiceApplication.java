package com.warehouse.gateway;

import com.netflix.appinfo.InstanceInfo;
import com.netflix.discovery.EurekaClient;
import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Profile;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.reactive.config.EnableWebFlux;

@SpringBootApplication
@EnableDiscoveryClient
@EnableWebFlux
@Log
public class GatewayServiceApplication {

    @Autowired
    private EurekaClient discoveryClient;

    public static void main(String[] args) {
        SpringApplication.run(GatewayServiceApplication.class, args);
    }

    @Bean
    @Profile("cloud")
    public RouteLocator customRouteLocator(RouteLocatorBuilder builder) {
        InstanceInfo orderService = discoveryClient.getNextServerFromEureka("ORDER-SERVICE", false);
        InstanceInfo stockService = discoveryClient.getNextServerFromEureka("STOCK-SERVICE", false);
        String orderServiceUri = String.format("http://%s:%s", orderService.getIPAddr(), orderService.getPort());
        String stockServiceUri = String.format("http://%s:%s", stockService.getIPAddr(), stockService.getPort());
        log.info("Order Service URI: " + orderServiceUri);
        log.info("Stock Service URI: " + stockServiceUri);
        return builder.routes()
                .route(p -> p
                        .path("/api/v1/warehouse/order")
                        .uri(orderServiceUri))
                .route(p -> p
                        .path("/api/v1/warehouse/order/*")
                        .uri(orderServiceUri))
                .route(p -> p
                        .path("/api/v1/warehouse/stock/*")
                        .uri(stockServiceUri))
                .build();
    }

    @Bean
    @LoadBalanced
    @Profile("cloud")
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }
}
