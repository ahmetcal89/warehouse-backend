package com.warehouse.order.api;

import com.warehouse.order.model.Order;
import com.warehouse.order.service.OrderService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.validation.Valid;

@Validated
@RestController
@RequestMapping("/api/v1/warehouse/order")
@CrossOrigin(origins = "*")
@Slf4j
public class OrderController {

    private final OrderService orderService;

    public OrderController(OrderService orderService) {
        this.orderService = orderService;
    }

    @PostMapping
    @PreAuthorize("hasAuthority('SCOPE_create:client_grants')")
    @ResponseStatus(HttpStatus.CREATED)
    public Mono<Order> create(@RequestBody @Valid Order order) {
        log.info("Creating a new {}", order);
        return orderService.createOrder(order);
    }

    @GetMapping("/{id}")
    @PreAuthorize("hasAuthority('SCOPE_read:client_grants')")
    @ResponseStatus(HttpStatus.OK)
    public Mono<Order> getById(@PathVariable Long id) {
        return orderService.findById(id);
    }

    @GetMapping
    @PreAuthorize("hasAuthority('SCOPE_read:client_grants')")
    @ResponseStatus(HttpStatus.OK)
    public Flux<Order> getAll() {
        return orderService.findAll();
    }
}
