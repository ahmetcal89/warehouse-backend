package com.warehouse.order.event;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.warehouse.order.service.OrderService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class OrderCanceledEventHandler {
    private final ObjectMapper mapper;
    private final OrderService orderService;

    public OrderCanceledEventHandler(ObjectMapper mapper, OrderService orderService) {
        this.mapper = mapper;
        this.orderService = orderService;
    }

    @RabbitListener(queues = {"${queue.order-canceled}"})
    public void handleOrderCanceledEvent(@Payload String payload) throws JsonProcessingException {
        log.debug("Handling a refund order event {}", payload);
        OrderCanceledEvent event = mapper.readValue(payload, OrderCanceledEvent.class);
        orderService.cancelOrder(event.getOrder().getId());
    }
}
