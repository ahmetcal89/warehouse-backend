package com.warehouse.order.event;

import com.warehouse.order.model.Order;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@ToString
public class OrderDoneEvent {
    private String transactionId;
    private Order order;
}
