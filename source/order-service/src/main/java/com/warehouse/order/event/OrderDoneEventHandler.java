package com.warehouse.order.event;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.warehouse.order.service.OrderService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class OrderDoneEventHandler {

    private final ObjectMapper mapper;
    private final OrderService orderService;

    public OrderDoneEventHandler(ObjectMapper mapper, OrderService orderService) {
        this.mapper = mapper;
        this.orderService = orderService;
    }

    @RabbitListener(queues = {"${queue.order-done}"})
    public void handleOrderDoneEvent(@Payload String payload) throws JsonProcessingException {
        log.debug("Handling a order done event {}", payload);
        OrderDoneEvent event = mapper.readValue(payload, OrderDoneEvent.class);
        orderService.updateOrderAsDone(event.getOrder().getId());
    }
}
