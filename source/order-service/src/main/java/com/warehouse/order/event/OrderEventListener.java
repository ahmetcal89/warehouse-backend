package com.warehouse.order.event;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.transaction.event.TransactionPhase;
import org.springframework.transaction.event.TransactionalEventListener;

@Component
@Slf4j
public class OrderEventListener {
    private final RabbitTemplate rabbitTemplate;
    private final ObjectMapper mapper;
    private final String queueOrderCreateName;

    public OrderEventListener(RabbitTemplate rabbitTemplate,
                              ObjectMapper mapper, @Value("${queue.order-create}") String queueOrderCreateName) {
        this.rabbitTemplate = rabbitTemplate;
        this.mapper = mapper;
        this.queueOrderCreateName = queueOrderCreateName;
    }

    @Async
    @TransactionalEventListener(phase = TransactionPhase.AFTER_COMMIT)
    public void onCreateEvent(OrderCreatedEvent event) throws JsonProcessingException {
        log.info("Sending order created event to {}, event: {}", queueOrderCreateName, event);
        rabbitTemplate.convertAndSend(queueOrderCreateName, mapper.writeValueAsString(event));
    }
}
