package com.warehouse.order.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
@Table(name = "orders")
public class Order {
    @Id
    private Long id;

    @NotNull
    @Column("product_id")
    private Long productId;
    @NotNull
    private Long quantity;
    private OrderStatus status;

    public enum OrderStatus {
        NEW, DONE, CANCELED
    }
}
