package com.warehouse.order.service;

import com.warehouse.order.model.Order;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface OrderService {
    Mono<Order> createOrder(Order order);

    void updateOrderAsDone(Long orderId);

    void cancelOrder(Long orderId);

    Mono<Order> findById(Long id);

    Flux<Order> findAll();
}
