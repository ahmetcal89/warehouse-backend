package com.warehouse.order.service.impl;

import com.warehouse.order.event.OrderCreatedEvent;
import com.warehouse.order.model.Order;
import com.warehouse.order.repository.OrderRepository;
import com.warehouse.order.service.OrderService;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.MDC;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.Optional;
import java.util.UUID;

@Service
@Slf4j
public class DefaultOrderService implements OrderService {

    private final OrderRepository orderRepository;
    private final ApplicationEventPublisher publisher;

    public DefaultOrderService(OrderRepository orderRepository, ApplicationEventPublisher publisher) {
        this.orderRepository = orderRepository;
        this.publisher = publisher;
    }

    @Override
    public Mono<Order> createOrder(Order order) {
        order.setStatus(Order.OrderStatus.NEW);
        publish(order);
        log.info("Saving an order {}", order);
        return orderRepository.save(order);
    }


    @Override
    public Flux<Order> findAll() {
        return orderRepository.findAll();
    }

    @Override
    @Transactional
    public void updateOrderAsDone(Long orderId) {
        log.info("Updating Order {} to {}", orderId, Order.OrderStatus.DONE);
        Optional<Order> orderOptional = orderRepository.findById(orderId).blockOptional();
        if (orderOptional.isPresent()) {
            Order order = orderOptional.get();
            order.setStatus(Order.OrderStatus.DONE);
            orderRepository.save(order);
        } else {
            log.error("Cannot update Order to status {}, Order {} not found", Order.OrderStatus.DONE, orderId);
        }
    }

    @Override
    @Transactional
    public void cancelOrder(Long orderId) {
        log.info("Canceling Order {}", orderId);
        Optional<Order> optionalOrder = orderRepository.findById(orderId).blockOptional();
        if (optionalOrder.isPresent()) {
            Order order = optionalOrder.get();
            order.setStatus(Order.OrderStatus.CANCELED);
            orderRepository.save(order);
            log.info("Order {} was canceled", order.getId());
        } else {
            log.error("Cannot find an Order by transaction {}", orderId);
        }
    }

    @Override
    public Mono<Order> findById(Long id) {
        return orderRepository.findById(id);
    }

    private void publish(Order order) {
        String transactionId = UUID.randomUUID().toString();
        MDC.put("TransactionID", transactionId);
        OrderCreatedEvent event = new OrderCreatedEvent(transactionId, order);
        log.info("Publishing an order created event {}", event);
        publisher.publishEvent(event);
    }
}
