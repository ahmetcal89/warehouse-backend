package com.warehouse.order;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.core.DefaultOAuth2AuthenticatedPrincipal;
import org.springframework.security.oauth2.core.OAuth2AccessToken;
import org.springframework.security.oauth2.core.OAuth2AuthenticatedPrincipal;
import org.springframework.security.oauth2.server.resource.authentication.BearerTokenAuthentication;
import org.springframework.security.test.context.support.WithSecurityContext;
import org.springframework.security.test.context.support.WithSecurityContextFactory;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Retention(RetentionPolicy.RUNTIME)
@WithSecurityContext(factory = WithMockTokenSecurityContextFactory.class)
public @interface WithMockToken {
    String accessToken() default "88888888-8888-8888-8888-888888888888";

    String subject() default "99999999-9999-9999-9999-999999999999";

    String[] authorities() default {};
}


class WithMockTokenSecurityContextFactory implements WithSecurityContextFactory<WithMockToken> {

    @Override
    public SecurityContext createSecurityContext(WithMockToken annotation) {
        SecurityContext securityContext = SecurityContextHolder.createEmptyContext();
        Map<String, Object> attributes = new HashMap<>();
        attributes.putIfAbsent("sub", annotation.subject());
        List<GrantedAuthority> authorities = Arrays.stream(annotation.authorities())
                .map(authority -> (GrantedAuthority) new SimpleGrantedAuthority(authority)).collect(Collectors.toList());
        OAuth2AuthenticatedPrincipal principal = new DefaultOAuth2AuthenticatedPrincipal(attributes, authorities);
        OAuth2AccessToken credentials = new OAuth2AccessToken(OAuth2AccessToken.TokenType.BEARER, annotation.accessToken(), null, null);
        Authentication authentication = new BearerTokenAuthentication(principal, credentials, authorities);
        securityContext.setAuthentication(authentication);
        return securityContext;
    }
}