package com.warehouse.order.api;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.warehouse.order.WithMockToken;
import com.warehouse.order.model.Order;
import com.warehouse.order.repository.OrderRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.security.test.web.reactive.server.SecurityMockServerConfigurers;
import org.springframework.test.web.reactive.server.WebTestClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureWebTestClient
class OrderControllerTest {

    private final String orderApiUri = "/api/v1/warehouse/order";
    private final ObjectMapper mapper = new ObjectMapper();

    @MockBean
    private OrderRepository mockOrderRepository;
    @MockBean
    private ApplicationEventPublisher mockApplicationEventPublisher;
    @Autowired
    private WebTestClient webTestClient;

    @Test
    @WithMockToken(authorities = "SCOPE_create:client_grants")
    void createOrder_success() throws JsonProcessingException {
        // Arrange
        Order order = new Order();
        order.setProductId(Long.valueOf(1));
        order.setQuantity(Long.valueOf(2));
        when(mockOrderRepository.save(any())).thenAnswer(i -> Mono.just(i.getArguments()[0]));

        // Act
        String responseBody = webTestClient
                .mutateWith(SecurityMockServerConfigurers.csrf())
                .post()
                .uri(orderApiUri)
                .body(Mono.just(order), Order.class)
                .exchange()
                .expectStatus()
                .isCreated()
                .expectBody(String.class)
                .returnResult()
                .getResponseBody();

        // Assert
        assertNotNull(responseBody);
        JsonNode node = mapper.readTree(responseBody);
        assertEquals("NEW", node.get("status").asText());
    }

    @Test
    @WithMockToken(authorities = "SCOPE_create:client_grants")
    void createOrder_nullProduct() throws JsonProcessingException {
        // Arrange
        Order order = new Order();
        order.setProductId(null);
        order.setQuantity(Long.valueOf(2));

        // Act & Assert
        webTestClient
                .mutateWith(SecurityMockServerConfigurers.csrf())
                .post()
                .uri(orderApiUri)
                .body(Mono.just(order), Order.class)
                .exchange()
                .expectStatus()
                .isBadRequest();
    }

    @Test
    @WithMockToken(authorities = "SCOPE_create:client_grants")
    void createOrder_nullQuantity() throws JsonProcessingException {
        // Arrange
        Order order = new Order();
        order.setProductId(Long.valueOf(2));
        order.setQuantity(null);

        // Act & Assert
        webTestClient
                .mutateWith(SecurityMockServerConfigurers.csrf())
                .post()
                .uri(orderApiUri)
                .body(Mono.just(order), Order.class)
                .exchange()
                .expectStatus()
                .isBadRequest();
    }

    @Test
    @WithMockToken(authorities = "SCOPE_read:client_grants")
    void listOrders_success() throws JsonProcessingException {
        // Arrange
        when(mockOrderRepository.findAll()).thenReturn(Flux.empty());

        // Act
        String responseBody = webTestClient
                .mutateWith(SecurityMockServerConfigurers.csrf())
                .get()
                .uri(orderApiUri)
                .exchange()
                .expectStatus()
                .isOk()
                .expectBody(String.class)
                .returnResult()
                .getResponseBody();

        // Assert
        assertNotNull(responseBody);
        JsonNode node = mapper.readTree(responseBody);
        assertTrue(node.isArray());
    }

    @Test
    @WithMockToken(authorities = "SCOPE_read:client_grants")
    void listOrders_byId() throws JsonProcessingException {
        // Arrange
        Long testId = Long.valueOf(1);
        Order order = new Order();
        order.setId(testId);
        when(mockOrderRepository.findById(testId)).thenReturn(Mono.just(order));

        // Act
        String responseBody = webTestClient
                .mutateWith(SecurityMockServerConfigurers.csrf())
                .get()
                .uri(String.format("%s/%s", orderApiUri, testId))
                .exchange()
                .expectStatus()
                .isOk()
                .expectBody(String.class)
                .returnResult()
                .getResponseBody();

        // Assert
        assertNotNull(responseBody);
        JsonNode node = mapper.readTree(responseBody);
        assertEquals(Long.valueOf(1), node.get("id").asLong());
    }

}