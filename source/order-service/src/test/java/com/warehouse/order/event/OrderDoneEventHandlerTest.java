package com.warehouse.order.event;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.warehouse.order.model.Order;
import com.warehouse.order.service.OrderService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class OrderDoneEventHandlerTest {

    @Mock
    private ObjectMapper mockMapper;
    @Mock
    private OrderService mockOrderService;
    @InjectMocks
    private OrderDoneEventHandler sut;

    @Test
    void handleOrderDoneEvent() throws JsonProcessingException {
        // Arrange
        String eventPayload = "Test OrderCanceledEvent";
        Order order = new Order();
        order.setId(Long.valueOf(1));
        OrderDoneEvent testEvent = new OrderDoneEvent("testTxId", order);
        when(mockMapper.readValue(eventPayload, OrderDoneEvent.class)).thenReturn(testEvent);

        // Act
        sut.handleOrderDoneEvent(eventPayload);

        // Assert
        verify(mockOrderService).updateOrderAsDone(order.getId());
    }
}