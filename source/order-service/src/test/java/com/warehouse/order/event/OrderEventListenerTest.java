package com.warehouse.order.event;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.amqp.rabbit.core.RabbitTemplate;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


@ExtendWith(MockitoExtension.class)
class OrderEventListenerTest {
    private final String queueOrderCreateName = "TEST_QUE";
    @Mock
    private RabbitTemplate mockRabbitTemplate;
    @Mock
    private ObjectMapper mockMapper;
    private OrderEventListener sut;

    @BeforeEach
    void setUp() {
        sut = new OrderEventListener(mockRabbitTemplate, mockMapper, queueOrderCreateName);
    }

    @Test
    void onCreateEvent() throws JsonProcessingException {
        // Arrange
        OrderCreatedEvent testEvent = new OrderCreatedEvent();
        String serializedTestEvent = "Serialized OrderCreatedEvent";
        when(mockMapper.writeValueAsString(testEvent)).thenReturn(serializedTestEvent);

        // Act
        sut.onCreateEvent(testEvent);

        // Assert
        verify(mockRabbitTemplate).convertAndSend(queueOrderCreateName, serializedTestEvent);
    }

}