package com.warehouse.order.service.impl;

import com.warehouse.order.event.OrderCreatedEvent;
import com.warehouse.order.model.Order;
import com.warehouse.order.repository.OrderRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.context.ApplicationEventPublisher;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class DefaultOrderServiceTest {

    @Mock
    private OrderRepository mockOrderRepository;
    @Mock
    private ApplicationEventPublisher mockPublisher;
    @InjectMocks
    private DefaultOrderService sut;

    @Test
    void createOrder() {
        // Arrange
        Order order = new Order();
        when(mockOrderRepository.save(any())).thenAnswer(i -> Mono.just(i.getArguments()[0]));

        // Act
        Order result = sut.createOrder(order).block();

        // Assert
        assertEquals(Order.OrderStatus.NEW, result.getStatus());
        verify(mockPublisher).publishEvent(any(OrderCreatedEvent.class));
    }

    @Test
    void updateOrderAsDone() {
        // Arrange
        Long orderId = Long.valueOf(1);
        Order order = new Order();
        order.setId(orderId);
        when(mockOrderRepository.findById(orderId)).thenReturn(Mono.just(order));

        // Act
        sut.updateOrderAsDone(orderId);

        // Assert
        verify(mockOrderRepository).save(any(Order.class));
    }

    @Test
    void updateOrderAsDone_error() {
        // Arrange
        Long orderId = Long.valueOf(1);
        Order order = new Order();
        order.setId(orderId);
        when(mockOrderRepository.findById(orderId)).thenReturn(Mono.empty());

        // Act
        sut.updateOrderAsDone(orderId);

        // Assert
        verify(mockOrderRepository, times(0)).save(any(Order.class));
    }

    @Test
    void cancelOrder() {
        // Arrange
        Long orderId = Long.valueOf(1);
        Order order = new Order();
        order.setId(orderId);
        when(mockOrderRepository.findById(orderId)).thenReturn(Mono.just(order));

        // Act
        sut.cancelOrder(orderId);

        // Assert
        verify(mockOrderRepository).save(any(Order.class));
    }

    @Test
    void cancelOrder_error() {
        // Arrange
        Long orderId = Long.valueOf(1);
        Order order = new Order();
        order.setId(orderId);
        when(mockOrderRepository.findById(orderId)).thenReturn(Mono.empty());

        // Act
        sut.cancelOrder(orderId);

        // Assert
        verify(mockOrderRepository, times(0)).save(any(Order.class));
    }

    @Test
    void findById() {
        // Arrange
        Long orderId = Long.valueOf(1);
        Order order = new Order();
        order.setId(orderId);
        when(mockOrderRepository.findById(orderId)).thenReturn(Mono.just(order));

        // Act
        Order result = sut.findById(orderId).block();

        // Assert
        assertNotNull(result);
        verify(mockOrderRepository, times(1)).findById(orderId);
    }

    @Test
    void findAll() {
        // Arrange
        Long orderId = Long.valueOf(1);
        Order order = new Order();
        order.setId(orderId);
        when(mockOrderRepository.findAll()).thenReturn(Flux.just(order));

        // Act
        Flux<Order> result = sut.findAll().onBackpressureLatest();

        // Assert
        assertNotNull(result);
        assertEquals(1, result.collectList().block().size());
        verify(mockOrderRepository, times(1)).findAll();
    }
}