package com.warehouse.stock.api;

import com.warehouse.stock.api.model.ArticleBundle;
import com.warehouse.stock.api.model.ProductBundle;
import com.warehouse.stock.model.Article;
import com.warehouse.stock.model.Product;
import com.warehouse.stock.service.StockService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/v1/warehouse/stock")
@Slf4j
public class StockController {

    private final StockService stockService;

    public StockController(StockService stockService) {
        this.stockService = stockService;
    }

    @PostMapping("/products")
    @PreAuthorize("hasAuthority('SCOPE_update:client_grants')")
    @ResponseStatus(HttpStatus.CREATED)
    public Flux<Product> createProducts(@RequestBody @Valid ProductBundle productBundle) {
        log.info("Updating products");
        return stockService.createProducts(productBundle.getProducts());
    }

    @GetMapping("/products")
    @PreAuthorize("hasAuthority('SCOPE_read:client_grants')")
    @ResponseStatus(HttpStatus.OK)
    public Flux<Product> getAllProducts() {
        log.info("List product request");
        return stockService.findAllProducts();
    }

    @PostMapping("/inventory")
    @PreAuthorize("hasAuthority('SCOPE_update:client_grants')")
    @ResponseStatus(HttpStatus.CREATED)
    public Flux<Article> createArticles(@RequestBody @Valid ArticleBundle articleBundle) {
        log.info("Updating Articles");
        return stockService.createArticles(articleBundle.getArticles());
    }

    @GetMapping("/inventory")
    @PreAuthorize("hasAuthority('SCOPE_read:client_grants')")
    @ResponseStatus(HttpStatus.OK)
    public Flux<Article> getAllArticles() {
        log.info("List articles request");
        return stockService.findAllArticles();
    }

}
