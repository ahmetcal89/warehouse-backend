package com.warehouse.stock.api.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.warehouse.stock.model.Article;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ArticleBundle {
    @JsonProperty("inventory")
    @NotNull
    List<Article> articles;
}
