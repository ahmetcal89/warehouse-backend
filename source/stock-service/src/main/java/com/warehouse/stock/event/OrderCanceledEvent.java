package com.warehouse.stock.event;

import com.warehouse.stock.model.Order;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class OrderCanceledEvent {
    private final Order order;
    private String transactionId;
}
