package com.warehouse.stock.event;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.warehouse.stock.exception.StockException;
import com.warehouse.stock.service.StockService;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.MDC;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class OrderCreateHandler {
    private final ObjectMapper mapper;
    private final StockService stockService;

    public OrderCreateHandler(ObjectMapper mapper, StockService stockService) {
        this.mapper = mapper;
        this.stockService = stockService;
    }

    @RabbitListener(queues = {"${queue.order-create}"})
    public void handleOrderCreatedEvent(@Payload String payload) throws JsonProcessingException {
        log.info("Handling a created order event {}", payload);
        OrderCreatedEvent event = mapper.readValue(payload, OrderCreatedEvent.class);
        MDC.put("TransactionID", event.getTransactionId());
        event.getOrder().setTransactionId(event.getTransactionId());
        try {
            stockService.updateStock(event.getOrder());
        } catch (StockException e) {
            log.error("Cannot update stock, reason: {}", e.getMessage());
        }
    }
}
