package com.warehouse.stock.event;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.transaction.event.TransactionPhase;
import org.springframework.transaction.event.TransactionalEventListener;

@Component
@Slf4j
public class StockEventListener {
    private static final String ROUTING_KEY = "";

    private final RabbitTemplate rabbitTemplate;
    private final ObjectMapper mapper;
    private final String queueOrderDoneName;
    private final String topicOrderCanceledName;

    public StockEventListener(RabbitTemplate rabbitTemplate,
                              ObjectMapper mapper, @Value("${queue.order-done}") String queueOrderDoneName,
                              @Value("${topic.order-canceled}") String topicOrderCanceledName) {
        this.rabbitTemplate = rabbitTemplate;
        this.mapper = mapper;
        this.queueOrderDoneName = queueOrderDoneName;
        this.topicOrderCanceledName = topicOrderCanceledName;
    }

    @Async
    @TransactionalEventListener(phase = TransactionPhase.AFTER_COMMIT)
    public void onOrderDoneEvent(OrderDoneEvent event) throws JsonProcessingException {
        log.info("Sending order done event to {}, event: {}", queueOrderDoneName, event);
        rabbitTemplate.convertAndSend(queueOrderDoneName, mapper.writeValueAsString(event));
    }

    @Async
    @TransactionalEventListener(phase = TransactionPhase.AFTER_ROLLBACK)
    public void onOrderCancelledEvent(OrderCanceledEvent event) throws JsonProcessingException {
        log.info("Sending order canceled event to {}, event: {}", topicOrderCanceledName, event);
        rabbitTemplate.convertAndSend(topicOrderCanceledName, ROUTING_KEY, mapper.writeValueAsString(event));
    }
}
