package com.warehouse.stock.exception;

public class StockException extends RuntimeException {
    public StockException(String message) {
        super(message);
    }
}
