package com.warehouse.stock.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;

@Data
@NoArgsConstructor
@Table(name = "inventory")
public class Article {
    @Id
    @Null
    private Long id;
    @NotNull
    private Long art_id;
    @NotEmpty
    private String name;
    @NotNull
    private Long stock;
}
