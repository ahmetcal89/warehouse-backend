package com.warehouse.stock.model;

import lombok.Data;

@Data
public class Order {
    private Long id;
    private String transactionId;
    private Long productId;
    private Long quantity;
}
