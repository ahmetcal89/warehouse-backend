package com.warehouse.stock.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.relational.core.mapping.Table;

import javax.validation.constraints.NotNull;
import java.util.List;

@Data
@NoArgsConstructor
@Accessors(chain = true)
@Table(name = "products")
public class Product {
    @Id
    private Long id;
    @NotNull
    private String name;

    @Transient
    @JsonProperty("contain_articles")
    @NotNull
    private List<Recipe> containArticles;

}
