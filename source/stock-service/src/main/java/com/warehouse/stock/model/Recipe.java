package com.warehouse.stock.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class Recipe {
    @JsonProperty("art_id")
    @NotNull
    private Long articleId;
    @JsonProperty("amount_of")
    @NotNull
    private Long amountOf;
}
