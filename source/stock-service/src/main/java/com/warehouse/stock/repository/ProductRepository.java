package com.warehouse.stock.repository;

import com.warehouse.stock.model.Product;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Mono;


@Repository
public interface ProductRepository extends ReactiveCrudRepository<Product, Long> {

    @Query("select * from products where name = $1")
    Mono<Product> findByName(String name);
}
