package com.warehouse.stock.service;

import com.warehouse.stock.model.Article;
import com.warehouse.stock.model.Order;
import com.warehouse.stock.model.Product;
import reactor.core.publisher.Flux;

import java.util.List;

public interface StockService {
    void updateStock(Order order);

    Flux<Product> createProducts(List<Product> products);

    Flux<Article> createArticles(List<Article> articles);

    Flux<Product> findAllProducts();

    Flux<Article> findAllArticles();
}
