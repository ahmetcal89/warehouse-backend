package com.warehouse.stock.service.impl;

import com.warehouse.stock.event.OrderCanceledEvent;
import com.warehouse.stock.event.OrderDoneEvent;
import com.warehouse.stock.exception.StockException;
import com.warehouse.stock.model.Article;
import com.warehouse.stock.model.Order;
import com.warehouse.stock.model.Product;
import com.warehouse.stock.model.Recipe;
import com.warehouse.stock.repository.ArticleRepository;
import com.warehouse.stock.repository.ProductRepository;
import com.warehouse.stock.service.StockService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import reactor.core.publisher.Flux;

import java.util.List;
import java.util.Optional;

@Service
@Slf4j
public class DefaultStockService implements StockService {
    private final ArticleRepository articleRepository;
    private final ProductRepository productRepository;
    private final ApplicationEventPublisher publisher;

    public DefaultStockService(ArticleRepository articleRepository, ProductRepository productRepository, ApplicationEventPublisher publisher) {
        this.articleRepository = articleRepository;
        this.productRepository = productRepository;
        this.publisher = publisher;
    }

    @Override
    @Transactional
    public void updateStock(Order order) {
        log.debug("Start updating product {}", order.getProductId());
        Product product = getProduct(order);
        checkStock(order, product);
        updateStock(order, product);
        publishOrderDone(order);
    }

    @Override
    public Flux<Product> createProducts(List<Product> products) {
        //List<Product> filteredProducts = products.stream().filter(product ->
        //     productRepository.findByName(product.getName()).block() == null
        //).collect(Collectors.toList());
        return productRepository.saveAll(products);
    }

    @Override
    public Flux<Article> createArticles(List<Article> articles) {
        return articleRepository.saveAll(articles);
    }

    @Override
    public Flux<Product> findAllProducts() {
        return productRepository.findAll();
    }

    @Override
    public Flux<Article> findAllArticles() {
        return articleRepository.findAll();
    }

    private void updateStock(Order order, Product product) {
        log.info("Updating product {} with quantity {}", product.getId(), order.getQuantity());
        List<Recipe> recipeList = product.getContainArticles();
        recipeList.forEach(recipe -> {
            Article article = articleRepository.findById(recipe.getArticleId()).block();
            article.setStock(article.getStock() - recipe.getAmountOf() * order.getQuantity());
            articleRepository.save(article);
        });
    }

    private void publishOrderDone(Order order) {
        OrderDoneEvent event = new OrderDoneEvent(order, order.getTransactionId());
        log.info("Publishing order done event {}", event);
        publisher.publishEvent(event);
    }

    private void checkStock(Order order, Product product) {
        log.info("Checking, products available {}, products ordered {}", product.getName(), order.getQuantity());
        List<Recipe> recipeList = product.getContainArticles();
        recipeList.stream().forEach(recipe -> {
            Article article = articleRepository.findById(recipe.getArticleId()).block();
            if (article == null || order.getQuantity() * recipe.getAmountOf() > article.getStock()) {
                publishCanceledOrder(order);
                throw new StockException(String.format("Article %s out of stock!", recipe.getArticleId()));
            }
        });
    }

    private void publishCanceledOrder(Order order) {
        OrderCanceledEvent event = new OrderCanceledEvent(order, order.getTransactionId());
        log.info("Publishing canceled order event {}", event);
        publisher.publishEvent(event);
    }

    private Product getProduct(Order order) {
        Optional<Product> optionalProduct = productRepository.findById(order.getProductId()).blockOptional();
        return optionalProduct.orElseThrow(() -> {
            log.info("Cannot find a product {}", order.getProductId());
            publishCanceledOrder(order);
            throw new StockException("Cannot find a product " + order.getProductId());
        });
    }
}
