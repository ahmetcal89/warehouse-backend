package com.warehouse.stock.api;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.warehouse.stock.WithMockToken;
import com.warehouse.stock.api.model.ArticleBundle;
import com.warehouse.stock.api.model.ProductBundle;
import com.warehouse.stock.model.Article;
import com.warehouse.stock.model.Product;
import com.warehouse.stock.model.Recipe;
import com.warehouse.stock.repository.ArticleRepository;
import com.warehouse.stock.repository.ProductRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.security.test.web.reactive.server.SecurityMockServerConfigurers;
import org.springframework.test.web.reactive.server.WebTestClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;


//@ExtendWith(SpringExtension.class)
//@WebFluxTest(StockController.class)
//@Import(DefaultStockService.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureWebTestClient
class StockControllerTest {
    private final String stockApiUri = "/api/v1/warehouse/stock";
    private final ObjectMapper mapper = new ObjectMapper();

    @MockBean
    private ArticleRepository articleRepository;
    @MockBean
    private ProductRepository productRepository;
    @MockBean
    private ApplicationEventPublisher publisher;
    @Autowired
    private WebTestClient webTestClient;

    @Test
    @WithMockToken(authorities = "SCOPE_update:client_grants")
    void createProducts_success() throws JsonProcessingException {
        // Arrange
        Recipe recipe = new Recipe();
        recipe.setArticleId(Long.valueOf(1));
        recipe.setAmountOf(Long.valueOf(2));
        Product testProduct = new Product();
        testProduct.setName("testProduct");
        testProduct.setContainArticles(Arrays.asList(recipe));
        ProductBundle testBundle = ProductBundle.builder().products(Arrays.asList(testProduct)).build();
        when(productRepository.findByName(anyString())).thenReturn(Mono.empty());
        when(productRepository.saveAll(anyList())).thenAnswer(p -> {
            List<Product> products = (List<Product>) p.getArguments()[0];
            return Flux.fromIterable(products);
        });

        // Act
        String responseBody = webTestClient
                .mutateWith(SecurityMockServerConfigurers.csrf())
                .post()
                .uri(stockApiUri + "/products")
                .body(Mono.just(testBundle), ProductBundle.class)
                .exchange()
                .expectStatus()
                .isCreated()
                .expectBody(String.class)
                .returnResult()
                .getResponseBody();

        // Assert
        assertNotNull(responseBody);
        JsonNode node = mapper.readTree(responseBody);
        assertNotNull(node.get(0).get("id").asText());
        assertEquals(testProduct.getName(), node.get(0).get("name").asText());
    }

    @Test
    @WithMockToken(authorities = "SCOPE_read:client_grants")
    void listProducts_success() throws JsonProcessingException {
        // Arrange
        when(productRepository.findAll()).thenReturn(Flux.empty());

        // Act
        String responseBody = webTestClient
                .mutateWith(SecurityMockServerConfigurers.csrf())
                .get()
                .uri(stockApiUri + "/products")
                .exchange()
                .expectStatus()
                .isOk()
                .expectBody(String.class)
                .returnResult()
                .getResponseBody();

        // Assert
        assertNotNull(responseBody);
        JsonNode node = mapper.readTree(responseBody);
        assertTrue(node.isArray());
    }

    @Test
    @WithMockToken(authorities = "SCOPE_update:client_grants")
    void createArticles_success() throws JsonProcessingException {
        // Arrange
        Article testArticle = new Article();
        testArticle.setName("Leg");
        testArticle.setArt_id(Long.valueOf(1));
        testArticle.setStock(Long.valueOf(10));
        ArticleBundle testBundle = ArticleBundle.builder().articles(Arrays.asList(testArticle)).build();
        when(articleRepository.saveAll(any(List.class))).thenAnswer(p -> Flux.just(p.getArguments()[0]));

        // Act
        String responseBody = webTestClient
                .mutateWith(SecurityMockServerConfigurers.csrf())
                .post()
                .uri(stockApiUri + "/inventory")
                .body(Mono.just(testBundle), ArticleBundle.class)
                .exchange()
                .expectStatus()
                .isCreated()
                .expectBody(String.class)
                .returnResult()
                .getResponseBody();

        // Assert
        assertNotNull(responseBody);
        JsonNode node = mapper.readTree(responseBody);
        assertEquals(testArticle.getArt_id(), node.get(0).get(0).get("art_id").asLong());
        assertEquals(testArticle.getName(), node.get(0).get(0).get("name").asText());
    }

    @Test
    @WithMockToken(authorities = "SCOPE_read:client_grants")
    void listArticles_success() throws JsonProcessingException {
        // Arrange
        when(articleRepository.findAll()).thenReturn(Flux.empty());

        // Act
        String responseBody = webTestClient
                .mutateWith(SecurityMockServerConfigurers.csrf())
                .get()
                .uri(stockApiUri + "/inventory")
                .exchange()
                .expectStatus()
                .isOk()
                .expectBody(String.class)
                .returnResult()
                .getResponseBody();

        // Assert
        assertNotNull(responseBody);
        JsonNode node = mapper.readTree(responseBody);
        assertTrue(node.isArray());
    }

}