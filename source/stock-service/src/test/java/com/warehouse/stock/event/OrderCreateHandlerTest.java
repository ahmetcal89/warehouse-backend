package com.warehouse.stock.event;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.warehouse.stock.model.Order;
import com.warehouse.stock.service.StockService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class OrderCreateHandlerTest {

    @Mock
    private ObjectMapper mockMapper;
    @Mock
    private StockService mockStockService;
    @InjectMocks
    private OrderCreateHandler sut;

    @Test
    void handle() throws JsonProcessingException {
        String eventPayload = "Test OrderCanceledEvent";
        Order order = new Order();
        order.setId(Long.valueOf(1));
        OrderCreatedEvent testEvent = new OrderCreatedEvent("", order);
        when(mockMapper.readValue(eventPayload, OrderCreatedEvent.class)).thenReturn(testEvent);

        // Act
        sut.handleOrderCreatedEvent(eventPayload);

        // Assert
        verify(mockStockService).updateStock(order);
    }

}