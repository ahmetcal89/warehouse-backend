package com.warehouse.stock.event;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.warehouse.stock.model.Order;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.amqp.rabbit.core.RabbitTemplate;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


@ExtendWith(MockitoExtension.class)
class StockEventListenerTest {
    private final String queueOrderDoneName = "TestOrderQue";
    private final String topicOrderCanceledName = "TestOrderCanceledTopic";

    @Mock
    private RabbitTemplate mockRabbitTemplate;
    @Mock
    private ObjectMapper mockMapper;
    private StockEventListener sut;

    @BeforeEach
    void setUp() {
        sut = new StockEventListener(mockRabbitTemplate, mockMapper, queueOrderDoneName, topicOrderCanceledName);
    }


    @Test
    void onOrderDoneEvent() throws JsonProcessingException {
        // Arrange
        OrderDoneEvent testEvent = new OrderDoneEvent(new Order(), "id");
        when(mockMapper.writeValueAsString(any())).thenReturn("Serialized OrderDoneEvent");

        // Act
        sut.onOrderDoneEvent(testEvent);

        // Assert
        verify(mockMapper).writeValueAsString(any());
        verify(mockRabbitTemplate).convertAndSend(any(String.class), any(String.class));
    }

    @Test
    void onOrderCancelledEvent() throws JsonProcessingException {
        // Arrange
        OrderCanceledEvent testEvent = new OrderCanceledEvent(new Order(), "id");
        when(mockMapper.writeValueAsString(any())).thenReturn("Serialized OrderCanceledEvent");

        // Act
        sut.onOrderCancelledEvent(testEvent);

        // Assert
        verify(mockMapper).writeValueAsString(any());
        verify(mockRabbitTemplate).convertAndSend(any(String.class), any(String.class), any(String.class));
    }
}