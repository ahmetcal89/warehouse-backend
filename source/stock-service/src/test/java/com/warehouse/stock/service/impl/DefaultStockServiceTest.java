package com.warehouse.stock.service.impl;

import com.warehouse.stock.event.OrderCanceledEvent;
import com.warehouse.stock.event.OrderDoneEvent;
import com.warehouse.stock.exception.StockException;
import com.warehouse.stock.model.Article;
import com.warehouse.stock.model.Order;
import com.warehouse.stock.model.Product;
import com.warehouse.stock.model.Recipe;
import com.warehouse.stock.repository.ArticleRepository;
import com.warehouse.stock.repository.ProductRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.context.ApplicationEventPublisher;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class DefaultStockServiceTest {

    @Mock
    private ArticleRepository mockArticleRepository;
    @Mock
    private ProductRepository mockProductRepository;
    @Mock
    private ApplicationEventPublisher mockPublisher;
    @InjectMocks
    private DefaultStockService sut;

    private Product testProduct;
    private Article testArticle;

    @BeforeEach
    void setUp() {
        Recipe recipe = new Recipe();
        recipe.setArticleId(Long.valueOf(1));
        recipe.setAmountOf(Long.valueOf(2));
        testProduct = new Product();
        testProduct.setName("table");
        testProduct.setContainArticles(Arrays.asList(recipe));

        testArticle = new Article();
        testArticle.setName("Leg");
        testArticle.setId(Long.valueOf(1));
        testArticle.setStock(Long.valueOf(10));
    }

    @Test
    void updateStock_orderDone() {
        // Arrange
        Order testOrder = new Order();
        testOrder.setId(Long.valueOf(1));
        testOrder.setProductId(testProduct.getId());
        testOrder.setQuantity(Long.valueOf(2));
        when(mockProductRepository.findById(testProduct.getId())).thenReturn(Mono.just(testProduct));
        when(mockArticleRepository.findById(testProduct.getContainArticles().get(0).getArticleId())).thenReturn(Mono.just(testArticle));

        // Act
        sut.updateStock(testOrder);

        // Assert
        verify(mockArticleRepository).save(any(Article.class));
        verify(mockPublisher).publishEvent(any(OrderDoneEvent.class));
    }

    @Test
    void updateStock_orderCanceled() {
        // Arrange
        Order testOrder = new Order();
        testOrder.setId(Long.valueOf(1));
        testOrder.setProductId(testProduct.getId());
        testOrder.setQuantity(Long.valueOf(2));
        when(mockProductRepository.findById(testProduct.getId())).thenReturn(Mono.just(testProduct));
        when(mockArticleRepository.findById(testProduct.getContainArticles().get(0).getArticleId())).thenReturn(Mono.empty());

        // Act
        assertThrows(StockException.class, () -> sut.updateStock(testOrder));

        // Assert
        verify(mockPublisher).publishEvent(any(OrderCanceledEvent.class));
    }

    @Test
    void createProducts() {
        // Arrange
        when(mockProductRepository.saveAll(anyList())).thenAnswer(p -> Flux.just(p.getArguments()[0]));

        // Act
        List<Product> result = sut.createProducts(Arrays.asList(testProduct)).collectList().block();

        // Assert
        assertNotNull(result);
        verify(mockProductRepository).saveAll(anyList());
    }

    @Test
    void createArticles() {
        // Act
        sut.createArticles(Arrays.asList(testArticle));
        // Assert
        //verify(mockArticleRepository).saveAll(any());
    }

    @Test
    void findAllProducts() {
        sut.findAllProducts();
        verify(mockProductRepository).findAll();
    }

    @Test
    void findAllArticles() {
        sut.findAllArticles();
        verify(mockArticleRepository).findAll();
    }
}